Shawn's Useful Windows Scripts
===============================

* `tpull.cmd` - For use with Git-TFS. Pulls from TFS into the local repository's _master_ branch, then merges into _dev_.
* `uptools.cmd`- Performs a `cup -y all` after updating the 32-bit version of Notepad++ (The 64-bit version doesn't
				 support the Plugin Manager, and Chocolatey will install the 64-bit version unless forced to 32-bit.)
* `EnableFusionLog.reg` - Turns on assembly binding logs.